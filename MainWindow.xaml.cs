﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMInventory
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ReceivingMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.Home());
        }

        private void ShippingDocumentMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.Shippings.ShippingDocuments());
        }

        private void RePrintBarcodeMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.RePrintBarcodePage());
        }

        private void WarehouseMenu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Form.WarehousePage());
        }

        private void RPT001Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT001());
        }

        private void RPT002Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT002());
        }

        private void RPT003Menu_Click(object sender, RoutedEventArgs e)
        {
            this.MainFrame.Navigate(new Report.RPT003());
        }
    }
}
