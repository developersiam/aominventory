﻿using AOMInventory.DAL;
using AOMInventory.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMInventory.Helper
{
    public static class TransportationHelper
    {
        public static List<vm_TransportationDocument> GetByCrop(short crop)
        {
            return (BusinessLayerService.ReceivingBLL().GetByCrop(crop).
                GroupBy(r => r.TransportationCode).
                Select(t => new vm_TransportationDocument
                {
                    Crop = t.First().Crop,
                    TransportationTruckNo = t.First().TransportationTruckNo,
                    TransportationCode = t.First().TransportationCode,
                    TotalWeight = Convert.ToDouble(t.Sum(x => x.BuyingWeight)),
                    TotalBales = Convert.ToDouble(t.Count()),
                    Received = Convert.ToDouble(t.Count(x => x.ReceivedDate != null)),
                    NotReceived = Convert.ToDouble(t.Count()) - Convert.ToDouble(t.Count(x => x.ReceivedDate != null)),
                    TotalShipped = Convert.ToDouble(t.Count(x => x.ShippingCode != null))
                })).ToList();
        }
    }
}
