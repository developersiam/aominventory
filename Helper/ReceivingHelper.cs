﻿using AOMInventory.DAL;
using AOMInventory.DomainModel;
using AOMInventory.ViewModel;
using Spire.Xls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMInventory.Helper
{
    public static class ReceivingHelper
    {
        public static List<vm_Receiving> GetByWarehouse(short crop, string tobaccoType, Guid? warehouseID)
        {
            var receivingList = BusinessLayerService.ReceivingBLL().GetByCrop(crop)
                .Where(x => x.ReceivedBy != null && x.ShippingCode == null).ToList();

            if (!string.IsNullOrEmpty(tobaccoType))
            {
                var temp = receivingList.Where(w => w.ProjectTypeID == tobaccoType).ToList();
                receivingList = temp;
            }

            if (warehouseID != Guid.Empty)
            {
                var temp = receivingList.Where(w => w.WarehouseID == warehouseID).ToList();
                receivingList = temp;
            }

            return receivingList.Select(s => new vm_Receiving
            {
                BaleBarcode = s.BaleBarcode,
                BaleNumber = s.BaleNumber,
                Crop = s.Crop,
                FarmerID = s.FarmerID,
                DocumentNumber = s.DocumentNumber,
                ProjectTypeID = s.ProjectTypeID,
                BuyingGradeCrop = s.BuyingGradeCrop,
                BuyingGrade = s.BuyingGrade,
                UnitPrice = s.UnitPrice,
                BuyingWeight = s.BuyingWeight,
                TransportationCode = s.TransportationCode,
                ReceivingWeight = s.ReceivingWeight,
                ReceivedDate = s.ReceivedDate,
                ReceivedBy = s.ReceivedBy,
                ShippingCode = s.ShippingCode,
                WarehouseID = s.WarehouseID,
                WarehouseName = s.Warehouse.WarehouseName,
                ImportBy = s.ImportBy,
                ImportDate = s.ImportDate
            }).ToList();
        }

        public static List<Receiving> ExcelTransform(string FilePath)
        {
            Workbook wb = new Workbook();
            wb.LoadFromFile(FilePath);
            Worksheet sheet = wb.Worksheets[0];
            var collection = DataTableToList(sheet.ExportDataTable());
            return collection;
        }

        public static List<Receiving> DataTableToList(DataTable Datable)
        {
            List<Receiving> receivingList = new List<Receiving>();
            foreach (DataRow row in Datable.Rows)
            {
                if (row["BaleBarcode"].ToString() == "")
                    break;

                receivingList.Add(new Receiving
                {
                    BaleBarcode = row["BaleBarcode"].ToString(),
                    BaleNumber = Convert.ToInt32(row["BaleNumber"]),
                    Crop = Convert.ToInt16(row["Crop"].ToString()),
                    FarmerID = row["FarmerID"].ToString(),
                    DocumentNumber = Convert.ToInt16(row["DocumentNumber"].ToString()),
                    BuyingDocumentCode = row["BuyingDocumentCode"].ToString(),
                    ProjectTypeID = row["ProjectTypeID"].ToString(),
                    RegisterBarcodeDate = DateTime.Parse(row["RegisterBarcodeDate"].ToString()),
                    BuyingGradeCrop = Convert.ToInt16(row["BuyingGradeCrop"].ToString()),
                    BuyingGrade = row["BuyingGrade"].ToString(),
                    BuyingGradeRecordDate = DateTime.Parse(row["BuyingGradeRecordDate"].ToString()),
                    BuyingWeight = Convert.ToDecimal(row["BuyingWeight"].ToString()),
                    BuyingWeightRecordDate = DateTime.Parse(row["BuyingWeightRecordDate"].ToString()),
                    LoadBaleToTruckDate = DateTime.Parse(row["LoadBaleToTruckDate"].ToString()),
                    UnitPrice = Convert.ToDecimal(row["UnitPrice"].ToString()),
                    TransportationCode = row["TransportationCode"].ToString(),
                    TransportationTruckNo = row["TruckNo"].ToString(),
                    ReceivedDate = null,
                    ReceivedBy = null,
                    ShippingCode = null,
                    ReceivingWeight = null,
                    ShippingToTruckDate = null,
                    ShippingToTruckUser = null
                });
            }
            return receivingList;
        }
    }
}
