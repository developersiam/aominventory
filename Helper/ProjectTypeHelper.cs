﻿using AOMInventory.DAL;
using AOMInventory.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMInventory.Helper
{
    public class ProjectTypeHelper
    {
        public static List<vm_Type> GetByCrop(short crop)
        {
            return BusinessLayerService.ReceivingBLL()
                .GetByCrop(crop).GroupBy(x => x.ProjectTypeID)
                .Select(x => new vm_Type { ProjectTypeID = x.Key })
                .ToList();
        }
    }
}
