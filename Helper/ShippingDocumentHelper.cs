﻿using AOMInventory.DAL;
using AOMInventory.ViewModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMInventory.Helper
{
    public static class ShippingDocumentHelper
    {
        public static vm_ShippingDocument GetSingle(string shippingCode)
        {
            var model = BusinessLayerService.ShippingDocumentBLL().GetSingle(shippingCode);

            return new vm_ShippingDocument
            {
                ShippingCode = model.ShippingCode,
                Crop = model.Crop,
                CreateDate = model.CreateDate,
                CreateUser = model.CreateUser,
                TruckNo = model.TruckNo,
                IsFinish = model.IsFinish,
                FinishDate = model.FinishDate,
                ModifiedDate = model.ModifiedDate,
                ModifiedUser = model.ModifiedUser,
                TotalBales = model.Receivings.Count(),
                TotalWeight = Convert.ToDouble(model.Receivings.Sum(r => r.BuyingWeight))
            };
        }

        public static List<vm_ShippingDocument> GetByCrop(short crop)
        {
            return (BusinessLayerService.ShippingDocumentBLL().GetAll(crop).
                GroupBy(s => s.ShippingCode).
                Select(t => new vm_ShippingDocument
                {
                    ShippingCode = t.FirstOrDefault().ShippingCode,
                    Crop = t.First().Crop,
                    CreateDate = t.First().CreateDate,
                    CreateUser = t.First().CreateUser,
                    TruckNo = t.First().TruckNo,
                    IsFinish = t.First().IsFinish,
                    FinishDate = t.First().FinishDate,
                    ModifiedDate = t.First().ModifiedDate,
                    ModifiedUser = t.First().ModifiedUser,
                    TotalBales = t.FirstOrDefault().Receivings.Count(),
                    TotalWeight = Convert.ToDouble(t.FirstOrDefault().Receivings.Sum(r => r.BuyingWeight))
                })).ToList();
        }

        public static string ExportTransportationDocument(string shippingCode, string folderName)
        {
            vm_ShippingDocument shippingDocumentViewModel = new vm_ShippingDocument();
            shippingDocumentViewModel = GetSingle(shippingCode);

            string name = @"" + folderName + "\\" + shippingCode + ".xlsx";
            FileInfo info = new FileInfo(name);

            List<vm_Shipping> list = new List<vm_Shipping>();
            list = Helper.ShippingHelper.GetByShippingCode(shippingCode);

            if (info.Exists)
                File.Delete(name);

            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add(shippingDocumentViewModel.ShippingCode);

                ws.Cells["A1"].LoadFromCollection(list, true);
                ws.Protection.IsProtected = false;

                using (ExcelRange col = ws.Cells[2, 10, 2 + list.Count, 10])
                {
                    col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm";
                }

                using (ExcelRange col = ws.Cells[2, 13, 2 + list.Count, 13])
                {
                    col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm";
                }

                using (ExcelRange col = ws.Cells[2, 15, 2 + list.Count, 15])
                {
                    col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm";
                }

                using (ExcelRange col = ws.Cells[2, 16, 2 + list.Count, 16])
                {
                    col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm";
                }

                using (ExcelRange col = ws.Cells[2, 20, 2 + list.Count, 20])
                {
                    col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm";
                }

                using (ExcelRange col = ws.Cells[2, 21, 2 + list.Count, 21])
                {
                    col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm";
                }

                using (ExcelRange col = ws.Cells[2, 26, 2 + list.Count, 26])
                {
                    col.Style.Numberformat.Format = "dd/MM/yyyy HH:mm";
                }

                pck.Save();
            }

            return @"" + folderName;
        }
    }
}
