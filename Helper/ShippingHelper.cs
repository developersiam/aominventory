﻿using AOMInventory.DAL;
using AOMInventory.ViewModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMInventory.Helper
{
    public static class ShippingHelper
    {
        public static List<vm_Shipping> GetByShippingCode(string shippingCode)
        {
            return BusinessLayerService.ReceivingBLL().GetByShippingCode(shippingCode)
                .Select(s => new vm_Shipping
                {
                    BaleBarcode = s.BaleBarcode,
                    BaleNumber = s.BaleNumber,
                    Crop = s.Crop,
                    FarmerID = s.FarmerID,
                    DocumentNumber = s.DocumentNumber,
                    BuyingDocumentCode = s.BuyingDocumentCode,
                    ProjectTypeID = s.ProjectTypeID,
                    RegisterBarcodeDate = s.RegisterBarcodeDate,
                    BuyingGradeCrop = s.BuyingGradeCrop,
                    BuyingGrade = s.BuyingGrade,
                    BuyingGradeRecordDate = s.BuyingGradeRecordDate,
                    BuyingWeight = s.BuyingWeight,
                    BuyingWeightRecordDate = s.BuyingWeightRecordDate,
                    LoadBaleToTruckDate = s.LoadBaleToTruckDate,
                    UnitPrice = s.UnitPrice,
                    TransportationCode = s.TransportationCode,
                    TransportationTruckNo = s.TransportationTruckNo,
                    ImportBy = s.ImportBy,
                    ImportDate = s.ImportDate,
                    ReceivedDate = s.ReceivedDate,
                    ReceivingWeight = s.ReceivingWeight,
                    ReceivedBy = s.ReceivedBy,
                    WarehouseID = s.WarehouseID,
                    WarehouseName = s.Warehouse.WarehouseName,
                    ShippingCode = s.ShippingCode,
                    ShippingToTruckDate = s.ShippingToTruckDate,
                    ShippingToTruckUser = s.ShippingToTruckUser,
                    ShippingTruckNo = s.ShippingDocument.TruckNo
                }).ToList();
        }

        public static List<vm_Shipping> GetByCrop(short crop)
        {
            return BusinessLayerService.ReceivingBLL().GetByCrop(crop)
                .Select(s => new vm_Shipping
                {
                    BaleBarcode = s.BaleBarcode,
                    BaleNumber = s.BaleNumber,
                    Crop = s.Crop,
                    FarmerID = s.FarmerID,
                    DocumentNumber = s.DocumentNumber,
                    BuyingDocumentCode = s.BuyingDocumentCode,
                    ProjectTypeID = s.ProjectTypeID,
                    RegisterBarcodeDate = s.RegisterBarcodeDate,
                    BuyingGradeCrop = s.BuyingGradeCrop,
                    BuyingGrade = s.BuyingGrade,
                    BuyingGradeRecordDate = s.BuyingGradeRecordDate,
                    BuyingWeight = s.BuyingWeight,
                    BuyingWeightRecordDate = s.BuyingWeightRecordDate,
                    LoadBaleToTruckDate = s.LoadBaleToTruckDate,
                    UnitPrice = s.UnitPrice,
                    TransportationCode = s.TransportationCode,
                    TransportationTruckNo = s.TransportationTruckNo,
                    ImportBy = s.ImportBy,
                    ImportDate = s.ImportDate,
                    ReceivedDate = s.ReceivedDate,
                    ReceivingWeight = s.ReceivingWeight,
                    ReceivedBy = s.ReceivedBy,
                    WarehouseID = s.WarehouseID,
                    WarehouseName = s.Warehouse.WarehouseName,
                    ShippingCode = s.ShippingCode,
                    ShippingToTruckDate = s.ShippingToTruckDate,
                    ShippingToTruckUser = s.ShippingToTruckUser,
                    ShippingTruckNo = s.ShippingDocument.TruckNo
                }).ToList();
        }

        public static string ExportCropRawData(short crop, string folderName)
        {
            string name = @"" + folderName + "\\SUMMARY-RECEIVING-CY" + crop + "-" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xlsx";
            FileInfo info = new FileInfo(name);

            List<vm_Shipping> list = new List<vm_Shipping>();
            list = GetByCrop(crop);

            if (info.Exists)
                File.Delete(name);

            using (ExcelPackage pck = new ExcelPackage(info))
            {
                ExcelWorksheet ws = pck.Workbook.Worksheets.Add("REC-CY" + crop + "-" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day);
                ws.Cells["A1"].LoadFromCollection(list, true);
                ws.Protection.IsProtected = false;
                using (ExcelRange col = ws.Cells[2, 6, 2 + list.Count, 6]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 9, 2 + list.Count, 9]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 11, 2 + list.Count, 11]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 12, 2 + list.Count, 12]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 16, 2 + list.Count, 16]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                using (ExcelRange col = ws.Cells[2, 18, 2 + list.Count, 18]) { col.Style.Numberformat.Format = "dd/MM/yyyy"; }
                pck.Save();
            }
            return @"" + folderName;
        }
    }
}
