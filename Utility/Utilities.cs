﻿using System.Collections.Generic;
using System.Windows.Controls;

namespace AOMInventory.Utility
{
    public static class Utilities
    {
        public static DataGrid GridBinding(DataGrid GridView, IEnumerable<object> ItemDataSource)
        {
            GridView.ItemsSource = null;
            GridView.ItemsSource = ItemDataSource;
            return GridView;
        }
        public static System.Windows.Forms.OpenFileDialog InputFile(System.Windows.Forms.OpenFileDialog choofdlog, string Filter, int FilterIndex, bool Multiselect)
        {
            //Show file select dialog and get file name and location.
            choofdlog.Filter = Filter;
            choofdlog.FilterIndex = FilterIndex;
            choofdlog.Multiselect = Multiselect;
            return choofdlog;
        }
    }
}
