﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMInventory.DAL;
using AOMInventory.DomainModel;
using AOMInventory.Utility;
using System.Drawing.Printing;

namespace AOMInventory.Utility
{
    public class PrintBarcode
    {
        public void PrintBarcodeSticker(int baleNumber)
        {
            Receiving receiving = new Receiving();
            receiving = BusinessLayerService.ReceivingBLL().GetSingleByBaleNumber(baleNumber);

            if (receiving == null) throw new ArgumentException("Find data not found!");

            PrinterSettings setting = new PrinterSettings();
            TSCPrinter.openport(setting.PrinterName);
            TSCPrinter.setup("103", "76", "2.0", "1", "0", "0", "0");
            TSCPrinter.sendcommand("GAP  3 mm,0");
            TSCPrinter.sendcommand("DIRECTION 1");
            TSCPrinter.clearbuffer();

            //TSCPrinter.sendcommand("BAR 40,40,4,528");          //เส้นตั้ง หน้าสุด
            //TSCPrinter.sendcommand("BAR 776,40,4,528");          //เส้นตั้ง หลังสุด
            //TSCPrinter.sendcommand("BAR 40,40,736,4");          //เส้นนอน บนสุด
            //TSCPrinter.sendcommand("BAR 40,568,736,4");          //เส้นนอน ล่างสุด

            TSCPrinter.barcode("40", "20", "128", "120", "0", "0", "2", "2", receiving.BaleBarcode);
            TSCPrinter.windowsfont(40, 140, 30, 0, 0, 0, "arial", receiving.BaleBarcode);
            TSCPrinter.barcode("660", "552", "128", "120", "0", "270", "2", "2", receiving.BaleBarcode);
            //TSCPrinter.windowsfont(660, 520, 30, 90, 0, 0, "arial", buying.BaleBarcode);

            TSCPrinter.windowsfont(80, 200, 80, 0, 0, 0, "arial", "ID : " + receiving.FarmerID);
            TSCPrinter.windowsfont(80, 280, 40, 0, 0, 0, "arial", "Name : " + "-");

            TSCPrinter.windowsfont(80, 360, 40, 0, 0, 0, "arial", "CY : " + receiving.Crop);
            TSCPrinter.windowsfont(80, 400, 40, 0, 0, 0, "arial", "Type : " + receiving.ProjectTypeID);
            TSCPrinter.windowsfont(80, 440, 40, 0, 0, 0, "arial", "Bale No : " + receiving.BaleNumber);

            TSCPrinter.windowsfont(80, 480, 40, 0, 0, 0, "arial", "Regis Date : " 
                + receiving.RegisterBarcodeDate.ToShortDateString() 
                + " " + receiving.RegisterBarcodeDate.ToLongTimeString());
            
            TSCPrinter.printlabel("1", "1");
            TSCPrinter.closeport();
        }
    }
}
