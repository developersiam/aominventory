﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMInventory.DomainModel;
using System.Data.Entity;

namespace DAL
{
    public class UnitOfWork : IUnitOfWork, System.IDisposable
    {
        private readonly AOMInventory.DAL.AOMInventorySystemEntities _context;
        private IGenericDataRepository<Crop> _cropRepository;
        private IGenericDataRepository<Receiving> _receivingRepository;
        private IGenericDataRepository<ShippingDocument> _shippingDocumentRepository;
        private IGenericDataRepository<Warehouse> _warehouseRepository;

        public UnitOfWork()
        {
            _context = new AOMInventory.DAL.AOMInventorySystemEntities();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }


        public IGenericDataRepository<Crop> CropRepository
        {
            get
            {
                return _cropRepository ?? (_cropRepository = new GenericDataRepository<Crop>(_context));
            }
        }

        public IGenericDataRepository<Receiving> ReceivingRepository
        {
            get
            {
                return _receivingRepository ?? (_receivingRepository = new GenericDataRepository<Receiving>(_context));
            }
        }

        public IGenericDataRepository<ShippingDocument> ShippingDocumentRepository
        {
            get
            {
                return _shippingDocumentRepository ?? (_shippingDocumentRepository = new GenericDataRepository<ShippingDocument>(_context));
            }
        }

        public IGenericDataRepository<Warehouse> WarehouseRepository
        {
            get
            {
                return _warehouseRepository ?? (_warehouseRepository = new GenericDataRepository<Warehouse>(_context));
            }
        }
    }
}