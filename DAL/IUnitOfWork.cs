﻿using AOMInventory.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IUnitOfWork
    {
        IGenericDataRepository<Crop> CropRepository { get; }
        IGenericDataRepository<Receiving> ReceivingRepository { get; }
        IGenericDataRepository<ShippingDocument> ShippingDocumentRepository { get; }
        IGenericDataRepository<Warehouse> WarehouseRepository { get; }
        void Save();
    }
}
