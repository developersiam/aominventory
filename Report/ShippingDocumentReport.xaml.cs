﻿using AOMInventory.DAL;
using AOMInventory.Utility;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMInventory.Report
{
    /// <summary>
    /// Interaction logic for RPT001Page.xaml
    /// </summary>
    public partial class ShippingDocumentReport : Window
    {
        public ShippingDocumentReport(string shippingCode)
        {
            InitializeComponent();

            ShippingDocumentReportViewer.Reset();

            ReportDataSource ReceivingDataSource = new ReportDataSource();
            ReceivingDataSource.Value = Helper.ShippingHelper.GetByShippingCode(shippingCode);
            ReceivingDataSource.Name = "ShippingDataSet";

            ShippingDocumentReportViewer.LocalReport.DataSources.Add(ReceivingDataSource);

            ShippingDocumentReportViewer.LocalReport.ReportEmbeddedResource = "AOMInventory.Report.RDLC.RPTSHP001.rdlc";
            ShippingDocumentReportViewer.RefreshReport();
        }
    }
}
