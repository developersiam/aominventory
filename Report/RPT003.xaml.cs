﻿using AOMInventory.DAL;
using AOMInventory.DomainModel;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMInventory.Report
{
    /// <summary>
    /// Interaction logic for RPT003.xaml
    /// </summary>
    public partial class RPT003 : Page
    {
        public RPT003()
        {
            InitializeComponent();
            RenderReport();
        }

        private void RenderReport()
        {
            try
            {
                RPT003ReportViewer.Reset();
                ReportDataSource ReceivingDataSource = new ReportDataSource();
                ReceivingDataSource.Value = BusinessLayerService.ReceivingBLL().GetByCrop(sys_config.DefaultCrop.Crop1);
                ReceivingDataSource.Name = "ReceivingViewModelDataSet";
                RPT003ReportViewer.LocalReport.DataSources.Add(ReceivingDataSource);
                RPT003ReportViewer.LocalReport.ReportEmbeddedResource = "AOMInventory.Report.RDLC.RPT003.rdlc";
                RPT003ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
