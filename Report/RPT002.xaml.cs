﻿using AOMInventory.DAL;
using AOMInventory.DomainModel;
using AOMInventory.ViewModel;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMInventory.Report
{
    /// <summary>
    /// Interaction logic for RPT002.xaml
    /// </summary>
    public partial class RPT002 : Page
    {
        public RPT002()
        {
            InitializeComponent();
            TobaccoTypeComboBoxBinding();
            WarehouseComboBoxBinding();
            RenderReport();
        }

        private void TobaccoTypeComboBoxBinding()
        {
            try
            {
                if (TobaccoTypeComboBox.Items.Count > 0) return;

                List<vm_Type> itemList = new List<vm_Type>();
                itemList.Add(new vm_Type { ProjectTypeID = "All Tobacco Type" });
                itemList.AddRange(Helper.ProjectTypeHelper.GetByCrop(sys_config.DefaultCrop.Crop1));
                TobaccoTypeComboBox.ItemsSource = null;
                TobaccoTypeComboBox.ItemsSource = itemList;
                TobaccoTypeComboBox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void WarehouseComboBoxBinding()
        {
            try
            {
                if (WarehouseComboBox.Items.Count > 0) return;

                List<Warehouse> itemList = new List<Warehouse>();
                itemList.Add(new Warehouse { WarehouseName = "All Warehouse", WarehouseID = Guid.Empty });
                itemList.AddRange(BusinessLayerService.WarehouseBLL()
                                                  .GetAll()
                                                  .OrderBy(o => o.WarehouseName).ToList());
                WarehouseComboBox.ItemsSource = null;
                WarehouseComboBox.ItemsSource = itemList;
                WarehouseComboBox.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TobaccoTypeComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (TobaccoTypeComboBox.Text != "") RenderReport();
        }

        private void WarehouseComboBox_DropDownClosed(object sender, EventArgs e)
        {
            if (WarehouseComboBox.Text != "") RenderReport();
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            RenderReport();
        }

        private void RenderReport()
        {
            try
            {
                string tobaccoType = TobaccoTypeComboBox.Text == string.Empty ||
                                     TobaccoTypeComboBox.Text == "All Tobacco Type" ? null : TobaccoTypeComboBox.Text;
                Guid? warehouseID = WarehouseComboBox.Text == string.Empty ||
                                    WarehouseComboBox.Text == "All Warehouse" ? Guid.Empty : (Guid)WarehouseComboBox.SelectedValue;

                RPT002ReportViewer.Reset();
                ReportDataSource ReceivingDataSource = new ReportDataSource();
                ReceivingDataSource.Value = Helper.ReceivingHelper
                     .GetByWarehouse(sys_config.DefaultCrop.Crop1, tobaccoType, Guid.Parse(warehouseID.ToString()));
                ReceivingDataSource.Name = "ReceivingViewModelDataSet";
                RPT002ReportViewer.LocalReport.DataSources.Add(ReceivingDataSource);
                RPT002ReportViewer.LocalReport.ReportEmbeddedResource = "AOMInventory.Report.RDLC.RPT002.rdlc";
                RPT002ReportViewer.RefreshReport();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
