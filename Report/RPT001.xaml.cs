﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AOMInventory.Utility;
using Microsoft.Reporting.WinForms;
using AOMInventory.DAL;
using AOMInventory.DomainModel;

namespace AOMInventory.Report
{
    /// <summary>
    /// Interaction logic for RPT001.xaml
    /// </summary>
    public partial class RPT001 : Page
    {
        Crop _crop;
        public RPT001()
        {
            InitializeComponent();
            _crop = new Crop();
            _crop = BusinessLayerService.CropBLL().GetDefaultCrop();

            RPT001ReportViewer.Reset();

            ReportDataSource ReceivingDataSource = new ReportDataSource();
            ReceivingDataSource.Value = BusinessLayerService.ReceivingBLL().GetByCrop(_crop.Crop1);
            ReceivingDataSource.Name = "ReceivingDataSet";

            RPT001ReportViewer.LocalReport.DataSources.Add(ReceivingDataSource);
            RPT001ReportViewer.LocalReport.ReportEmbeddedResource = "AOMInventory.Report.RDLC.RPT001.rdlc";
            RPT001ReportViewer.RefreshReport();
        }
    }
}
