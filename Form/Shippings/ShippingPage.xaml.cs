﻿using AOMInventory.DAL;
using AOMInventory.DomainModel;
using AOMInventory.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMInventory.Form.Shippings
{
    /// <summary>
    /// Interaction logic for ShippingPage.xaml
    /// </summary>
    public partial class ShippingPage : Page
    {
        ShippingDocument _shippingDocument;
        Receiving _receiving;
        string _shippingCode;

        public ShippingPage(string shippingCode, string shippingDocumentCode)
        {
            InitializeComponent();
            _shippingCode = shippingCode;
            _shippingDocument = new ShippingDocument();
            _receiving = new Receiving();

            shippingCodeTextBox.Text = shippingDocumentCode;
            shippingRadioButton.IsChecked = true;
            ReloadPage();
        }

        private void ReloadPage()
        {
            try
            {
                _shippingDocument = BusinessLayerService.ShippingDocumentBLL().GetSingle(_shippingCode);

                shippingDocumentDetailDataGrid.ItemsSource = null;
                shippingDocumentDetailDataGrid.ItemsSource = _shippingDocument.Receivings.OrderByDescending(x => x.ShippingToTruckDate);

                totalItemLabel.Content = string.Format("{0:N0}", _shippingDocument.Receivings.Count());
                shippingRadioButton.IsChecked = true;

                if (_shippingDocument.IsFinish == false)
                {
                    finishDocumentButton.Visibility = Visibility.Visible;
                    unFinishDocumentButton.Visibility = Visibility.Collapsed;
                    printDocumentButton.IsEnabled = false;
                    exportToExcelButton.IsEnabled = false;
                }
                else
                {
                    finishDocumentButton.Visibility = Visibility.Collapsed;
                    unFinishDocumentButton.Visibility = Visibility.Visible;
                    printDocumentButton.IsEnabled = true;
                    exportToExcelButton.IsEnabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Save(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                {
                    MessageBox.Show("Please input a bale barcode.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    return;
                }

                if (_shippingDocument.IsFinish == true)
                    throw new ArgumentException("This shipping code was finished. The system cannot to do.");

                if (_receiving.ReceivedDate == null)
                    throw new ArgumentException("This bale imported to the system already. But it's not receive. The system cannot to do.");

                if (shippingRadioButton.IsChecked == true)
                {
                    /// ถ้าเกิดกรณีห่อยาที่เคยทำการ shipping ไปแล้วใน shipping code อื่นแล้วจะไม่ให้บันทึกข้อมูล
                    /// 
                    if (_receiving.ShippingCode != null)
                    {
                        if (_receiving.ShippingCode != shippingCodeTextBox.Text)
                        {
                            MessageBox.Show("This bale was shipped already in a shipping code " + _receiving.ShippingCode
                                + ". The system cannot to do this.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                            return;
                        }

                        if (_receiving.ReceivedDate == null)
                            throw new ArgumentException("This bale imported to the system already. But it's not scan to receive. The system cannot to do.");

                        if (MessageBox.Show("This bale already to shipping. Do you want to update again?", "confirmation",
                            MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                            return;
                    }
                    BusinessLayerService.ShippingBLL().BringUp(baleBarcode, sys_config.CurrentUser, _shippingCode);
                }
                else
                {
                    if (_receiving.ShippingCode == null)
                        throw new ArgumentException("This bale not record a shipping code. The system cannot to return.");

                    if (_receiving.ShippingCode != shippingCodeTextBox.Text)
                        throw new ArgumentException("The shipping code not same (" + _receiving.ShippingCode + "). the system cannot return the bale.");

                    BusinessLayerService.ShippingBLL().BringDown(baleBarcode);
                }

                ReloadPage();
                baleBarcodeTextBox.Text = "";
                baleNumberTextBox.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ShippingRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            takeOffRadioButton.IsChecked = false;
        }

        private void TakeOffRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            shippingRadioButton.IsChecked = false;
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter) return;

                if (baleBarcodeTextBox.Text.Trim() == "")
                {
                    MessageBox.Show("Please input a bale barcode.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    baleBarcodeTextBox.Clear();
                    return;
                }

                _receiving = BusinessLayerService.ReceivingBLL()
                    .GetSingleByBaleBarcode(baleBarcodeTextBox.Text.Replace(" ", string.Empty));

                if (_receiving == null)
                {
                    MessageBox.Show("Find a data not found!", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    baleBarcodeTextBox.Clear();
                    baleNumberTextBox.Clear();
                    return;
                }

                Save(_receiving.BaleBarcode);
                baleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter)
                    return;

                if (baleNumberTextBox.Text == "")
                {
                    MessageBox.Show("Please input a bale number.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleNumberTextBox.Focus();
                    baleNumberTextBox.Clear();
                    return;
                }

                _receiving = BusinessLayerService.ReceivingBLL()
                    .GetSingleByBaleNumber(Convert.ToInt32(baleNumberTextBox.Text.Replace(" ", string.Empty)));

                if (_receiving == null)
                {
                    MessageBox.Show("This bale not keep in the system!", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    baleBarcodeTextBox.Focus();
                    baleNumberTextBox.Clear();
                    baleBarcodeTextBox.Clear();
                    return;
                }
                Save(_receiving.BaleBarcode);
                baleNumberTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ReloadPage();
        }

        private void FinishDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BusinessLayerService.ShippingDocumentBLL().FinishDocument(_shippingCode);
                ReloadPage();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void UnFinishDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BusinessLayerService.ShippingDocumentBLL().UnFinishDocument(_shippingCode);
                ReloadPage();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Report.ShippingDocumentReport window = new Report.ShippingDocumentReport(_shippingCode);
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ExportToExcelButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_shippingCode == null)
                {
                    MessageBox.Show("The shipping code not found!", "Warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                System.Windows.Forms.FolderBrowserDialog folderDlg = new System.Windows.Forms.FolderBrowserDialog();
                folderDlg.ShowNewFolderButton = true;

                // Show the FolderBrowserDialog.
                System.Windows.Forms.DialogResult result = folderDlg.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    string folderPath = Helper.ShippingDocumentHelper
                        .ExportTransportationDocument(_shippingCode, folderDlg.SelectedPath);

                    Environment.SpecialFolder root = folderDlg.RootFolder;

                    MessageBox.Show("Export complete.", "Complete", MessageBoxButton.OK, MessageBoxImage.Information);

                    System.Diagnostics.Process.Start(folderPath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CancelReceivingButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shippingDocumentDetailDataGrid.SelectedIndex < 0)
                    return;

                var selectedItem = (Receiving)shippingDocumentDetailDataGrid.SelectedItem;
                _receiving = BusinessLayerService.ReceivingBLL().GetSingleByBaleBarcode(selectedItem.BaleBarcode);

                if (_shippingDocument.IsFinish == true)
                    throw new ArgumentException("This shipping code was finished. The system cannot to do.");

                if (_receiving.ShippingCode == null)
                    throw new ArgumentException("This bale not record a shipping code. The system cannot to return.");

                if (_receiving.ShippingCode != shippingCodeTextBox.Text)
                    throw new ArgumentException("The shipping code not same (" + _receiving.ShippingCode + "). the system cannot return the bale.");

                BusinessLayerService.ShippingBLL().BringDown(selectedItem.BaleBarcode);

                ReloadPage();
                baleBarcodeTextBox.Text = "";
                baleNumberTextBox.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Page_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Escape)
                return;

            baleBarcodeTextBox.Text = "";
            baleNumberTextBox.Text = "";
            shippingRadioButton.IsChecked = true;
            takeOffRadioButton.IsChecked = false;
            baleBarcodeTextBox.Focus();
        }
    }
}
