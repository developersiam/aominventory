﻿using AOMInventory.DAL;
using AOMInventory.DomainModel;
using AOMInventory.Utility;
using AOMInventory.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMInventory.Form.Shippings
{
    /// <summary>
    /// Interaction logic for ShippingDocumentPage.xaml
    /// </summary>
    public partial class ShippingDocuments : Page
    {
        vm_ShippingDocument _shipping;

        public ShippingDocuments()
        {
            InitializeComponent();
            createDatePicker.SelectedDate = DateTime.Now;
            ReloadShippingDocument();

            _shipping = new vm_ShippingDocument();
            _shipping = null;
        }

        private void ReloadShippingDocument()
        {
            try
            {
                shippingDocumentDataGrid.ItemsSource = null;
                shippingDocumentDataGrid.ItemsSource = Helper.ShippingDocumentHelper.GetByCrop(sys_config.DefaultCrop.Crop1);
                totalItemLabel.Content = string.Format("{0:N0}", shippingDocumentDataGrid.Items.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CreateDocument()
        {
            try
            {
                if (createDatePicker.SelectedDate == null)
                {
                    MessageBox.Show("Please select a create document date.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (truckNumberTextBox.Text == "")
                {
                    MessageBox.Show("Please specify a truck number.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    truckNumberTextBox.Focus();
                    return;
                }

                BusinessLayerService.ShippingDocumentBLL()
                    .Add(sys_config.DefaultCrop.Crop1,
                    truckNumberTextBox.Text,
                    Convert.ToDateTime(createDatePicker.Text),
                    sys_config.CurrentUser);

                ReloadShippingDocument();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                editButton.Visibility = Visibility.Collapsed;
                createDocumentButton.Visibility = Visibility.Visible;
                truckNumberTextBox.Clear();
                ReloadShippingDocument();
                _shipping = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CreateDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                CreateDocument();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ViewDetailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shippingDocumentDataGrid.SelectedIndex <= -1)
                    return;

                var selectedItem = (vm_ShippingDocument)shippingDocumentDataGrid.SelectedItem;
                this.NavigationService.Navigate(new ShippingPage(selectedItem.ShippingCode, selectedItem.ShippingCode));

                ReloadShippingDocument();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shippingDocumentDataGrid.SelectedIndex <= -1)
                    return;

                if (MessageBox.Show("Do you want to delete this item?", "Warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var selectedItem = (vm_ShippingDocument)shippingDocumentDataGrid.SelectedItem;
                // Delete item.
                BusinessLayerService.ShippingDocumentBLL().Delete(selectedItem.ShippingCode);

                ReloadShippingDocument();
                truckNumberTextBox.Clear();
                truckNumberTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void TruckNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                {
                    CreateDocument();
                    truckNumberTextBox.Clear();
                    truckNumberTextBox.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to edit this item?", "Warning!",
                    MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                if (truckNumberTextBox.Text == null)
                {
                    MessageBox.Show("Please input a truck number.", "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var model = BusinessLayerService.ShippingDocumentBLL().GetSingle(_shipping.ShippingCode);

                model.TruckNo = truckNumberTextBox.Text;
                model.ModifiedUser = sys_config.CurrentUser;
                model.ModifiedDate = DateTime.Now;

                BusinessLayerService.ShippingDocumentBLL().Update(model);

                ReloadShippingDocument();
                truckNumberTextBox.Clear();
                truckNumberTextBox.Focus();

                editButton.Visibility = Visibility.Collapsed;
                createDocumentButton.Visibility = Visibility.Visible;

                _shipping = null;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void EditOnDataGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (shippingDocumentDataGrid.SelectedIndex < 0)
                    return;

                _shipping = (vm_ShippingDocument)shippingDocumentDataGrid.SelectedItem;

                truckNumberTextBox.Text = _shipping.TruckNo;
                editButton.Visibility = Visibility.Visible;
                createDocumentButton.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
