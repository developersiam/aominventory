﻿using AOMInventory.DAL;
using AOMInventory.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMInventory.Form
{
    /// <summary>
    /// Interaction logic for WarehousePage.xaml
    /// </summary>
    public partial class WarehousePage : Page
    {
        Warehouse _warehouse;
        List<Warehouse> _warehouseList;

        public WarehousePage()
        {
            InitializeComponent();

            _warehouse = new Warehouse();
            _warehouseList = new List<Warehouse>();
        }

        private void ReloadWarehouseList()
        {
            try
            {
                _warehouseList = BusinessLayerService.WarehouseBLL().GetAll();

                WarehouseDataGrid.ItemsSource = null;
                WarehouseDataGrid.ItemsSource = _warehouseList.OrderBy(w => w.WarehouseName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearForm()
        {
            WarehouseNameTextBox.Text = "";
            WarehouseNameTextBox.Focus();
            addButton.Visibility = Visibility.Visible;
            editButton.Visibility = Visibility.Collapsed;

            _warehouse = null;
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClearForm();
                ReloadWarehouseList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editOnGridButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (WarehouseDataGrid.SelectedIndex < 0)
                    return;

                var selectedItem = (Warehouse)WarehouseDataGrid.SelectedItem;
                _warehouse = BusinessLayerService.WarehouseBLL().GetSingle(selectedItem.WarehouseID);

                WarehouseNameTextBox.Text = _warehouse.WarehouseName;
                editButton.Visibility = Visibility.Visible;
                addButton.Visibility = Visibility.Collapsed;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (WarehouseDataGrid.SelectedIndex < 0)
                    return;

                if (MessageBox.Show("Do you want to delete this item?", "warning!", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                    return;

                var selectedItem = (Warehouse)WarehouseDataGrid.SelectedItem;
                BusinessLayerService.WarehouseBLL().Delete(selectedItem.WarehouseID);

                ClearForm();
                ReloadWarehouseList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void addButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (WarehouseNameTextBox.Text == "")
                {
                    MessageBox.Show("Please input warehouse name.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    WarehouseNameTextBox.Focus();
                    return;
                }

                BusinessLayerService.WarehouseBLL()
                        .Add(WarehouseNameTextBox.Text.Replace(" ", string.Empty));

                ClearForm();
                ReloadWarehouseList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_warehouse == null)
                {
                    MessageBox.Show("Please select the row on the table again.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (WarehouseNameTextBox.Text == "")
                {
                    MessageBox.Show("Please input warehouse name.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    WarehouseNameTextBox.Focus();
                    return;
                }

                _warehouse.WarehouseName = WarehouseNameTextBox.Text;
                BusinessLayerService.WarehouseBLL().Edit(_warehouse);

                ClearForm();
                ReloadWarehouseList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            ClearForm();
            ReloadWarehouseList();
        }
    }
}
