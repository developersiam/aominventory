﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using AOMInventory.DomainModel;
using AOMInventory.DAL;

namespace AOMInventory.Form.Receivings
{
    /// <summary>
    /// Interaction logic for ReceivingDocumentDetailWindow.xaml
    /// </summary>
    public partial class TruckPackingLists : Window
    {
        private List<Receiving> _truckPackingList;

        public TruckPackingLists()
        {

            InitializeComponent();
            _truckPackingList = new List<Receiving>();
        }

        private System.Windows.Forms.OpenFileDialog InputFile(System.Windows.Forms.OpenFileDialog choofdlog,
            string Filter, int FilterIndex, bool Multiselect)
        {
            //Show file select dialog and get file name and location.
            choofdlog.Filter = Filter;
            choofdlog.FilterIndex = FilterIndex;
            choofdlog.Multiselect = Multiselect;
            return choofdlog;
        }

        private void DataGridBinding()
        {
            try
            {
                _truckPackingList = Helper.ReceivingHelper.ExcelTransform(filePathTextBox.Text);

                transportationInfoPanel.Visibility = Visibility.Visible;
                toolsButtonPanel.Visibility = Visibility.Visible;
                importAndReplaceButton.Visibility = Visibility.Collapsed;

                int totalDupplicate = 0;

                // เป็นการปรับค่าของ _list ที่ได้จากการอ่านข้อมูลจาก excel เพื่อนำไปแสดงบนหน้าจอว่า... ยาห่อใดบ้างที่เคยมีการ import ไปก่อนนี้แล้ว
                List<Receiving> dbList = new List<Receiving>();
                dbList = BusinessLayerService.ReceivingBLL()
                    .GetByCrop(sys_config.DefaultCrop.Crop1);

                foreach (var item in _truckPackingList)
                    item.ImportBy = "New bale";

                if (dbList.Count > 0)
                {
                    /// ปรับค่าใน column ImportBy เพื่อใช้ในการแสดงให้ user ทราบสถานะของการ import
                    foreach (var item in _truckPackingList)
                    {
                        var fromDB = dbList.SingleOrDefault(l => l.BaleBarcode == item.BaleBarcode);
                        if (fromDB != null && fromDB.ImportBy != null)
                        {
                            item.ImportBy = "Imported";
                            item.ImportDate = fromDB.ImportDate;
                            item.Warehouse = fromDB.Warehouse;
                            totalDupplicate++;
                        }
                    }
                }

                if (_truckPackingList.Where(x => x.ImportDate != null).Count() > 0)
                    importAndReplaceButton.Visibility = Visibility.Visible;

                truckPackingListDataGrid.ItemsSource = null;
                truckPackingListDataGrid.ItemsSource = _truckPackingList.OrderBy(t => t.ImportBy);

                transportationCodeTextBox.Text = _truckPackingList.FirstOrDefault().TransportationCode;
                WaitForImportTextBox.Text = string.Format("{0:N0}", _truckPackingList.Count - totalDupplicate);
                InTheSystemTextBox.Text = string.Format("{0:N0}", totalDupplicate);
                TotalBalesTextBox.Text = string.Format("{0:N0}", _truckPackingList.Count);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Import()
        {
            try
            {
                if (MessageBox.Show("You have a " + _truckPackingList.Where(x => x.ImportDate == null).Count() +
                    " bales wait for import and " + _truckPackingList.Where(x => x.ImportDate != null).Count() +
                    " bales was imported." + Environment.NewLine +
                    "Do you want to do that?", "warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                var importList = _truckPackingList.Where(x => x.ImportDate == null);

                foreach (var item in importList)
                {
                    item.ImportBy = sys_config.CurrentUser;
                    item.ImportDate = DateTime.Now;
                    BusinessLayerService.ReceivingBLL().Add(item);
                }

                MessageBox.Show("Imported done! " + _truckPackingList.Count() + " bales", "success",
                    MessageBoxButton.OK, MessageBoxImage.Information);

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ImportAndReplace()
        {
            try
            {
                if (MessageBox.Show("You have a " + _truckPackingList.Count() + " bales." + Environment.NewLine +
                    _truckPackingList.Where(x => x.ImportDate == null).Count() + " (new bales)" + Environment.NewLine +
                    _truckPackingList.Where(x => x.ImportDate != null).Count() + " (imported bales)" + Environment.NewLine +
                    "Do you want to do that?", "warning", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                var updateList = _truckPackingList.Where(x => x.ImportDate != null).ToList();
                var addList = _truckPackingList.Where(x => x.ImportDate == null).ToList();
                // Update the old records.
                foreach (var item in updateList)
                {
                    var edit = BusinessLayerService.ReceivingBLL()
                        .GetSingleByBaleBarcode(item.BaleBarcode);

                    edit.Crop = item.Crop;
                    edit.FarmerID = item.FarmerID;
                    edit.DocumentNumber = item.DocumentNumber;
                    edit.BuyingDocumentCode = item.BuyingDocumentCode;
                    edit.ProjectTypeID = item.ProjectTypeID;
                    edit.RegisterBarcodeDate = item.RegisterBarcodeDate;
                    edit.BuyingGradeCrop = item.BuyingGradeCrop;
                    edit.BuyingGrade = item.BuyingGrade;
                    edit.BuyingGradeRecordDate = item.BuyingGradeRecordDate;
                    edit.BuyingWeight = item.BuyingWeight;
                    edit.BuyingWeightRecordDate = item.BuyingWeightRecordDate;
                    edit.LoadBaleToTruckDate = item.LoadBaleToTruckDate;
                    edit.UnitPrice = item.UnitPrice;
                    edit.TransportationCode = item.TransportationCode;
                    edit.TransportationTruckNo = item.TransportationTruckNo;

                    edit.ImportBy = sys_config.CurrentUser;
                    edit.ImportDate = DateTime.Now;

                    BusinessLayerService.ReceivingBLL().Update(edit);
                }

                // Add a new records.
                foreach (var item in addList)
                {
                    item.ImportBy = sys_config.CurrentUser;
                    item.ImportDate = DateTime.Now;
                    BusinessLayerService.ReceivingBLL().Add(item);
                }

                MessageBox.Show("Import and replace done!", "success", MessageBoxButton.OK, MessageBoxImage.Information);
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void browseFileButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog choofdlog = new System.Windows.Forms.OpenFileDialog();
                InputFile(choofdlog, "Excel Files|*.xls;*.xlsx;*.xlsm", 1, false);
                if (choofdlog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                    filePathTextBox.Text = choofdlog.FileName;

                if (filePathTextBox.Text == "" || filePathTextBox.Text == null)
                    return;

                DataGridBinding();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void importButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (truckPackingListDataGrid.Items.Count <= 0)
                {
                    MessageBox.Show("this truck packing list not have a bale. cannot to import data!",
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                Import();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void importAndReplaceButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ImportAndReplace();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
