﻿using AOMInventory.DAL;
using AOMInventory.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AOMInventory.Form.Receivings
{
    /// <summary>
    /// Interaction logic for ReceivingWindow.xaml
    /// </summary>
    public partial class ReceivingWindow : Window
    {
        Receiving _receiving;
        List<Receiving> _truckPackingList;
        string _transportationCode;

        public ReceivingWindow(string transportationCode)
        {
            InitializeComponent();
            _receiving = new Receiving();
            _truckPackingList = new List<Receiving>();
            _transportationCode = transportationCode;

            WarehouseCombobox.ItemsSource = null;
            WarehouseCombobox.ItemsSource = BusinessLayerService.WarehouseBLL().GetAll().OrderBy(o => o.WarehouseName);
            ReceiveRadioButton.IsChecked = true;
            TakeOffRadioButton.IsChecked = false;
            ReloadPage();
        }

        private void ReloadPage()
        {
            try
            {
                BaleBarcodeTextBox.Clear();
                BaleNumberTextBox.Clear();

                _truckPackingList = BusinessLayerService.ReceivingBLL().GetByTransportationCode(_transportationCode);

                WaitForReceivingDataGrid.ItemsSource = null;
                WaitForReceivingDataGrid.ItemsSource = _truckPackingList
                    .Where(r => r.ReceivedBy == null)
                    .ToList()
                    .OrderBy(r => r.BaleNumber);

                ReceiviedDataGrid.ItemsSource = null;
                ReceiviedDataGrid.ItemsSource = _truckPackingList
                    .Where(r => r.ReceivedBy != null)
                    .ToList()
                    .OrderByDescending(r => r.ReceivedDate);

                TransportationCodeTextBox.Text = _truckPackingList.FirstOrDefault().TransportationCode;
                TotalTransportBalesTextBox.Text = string.Format("{0:N0}", _truckPackingList.Count);
                TotalWaitForReceivingTextBox.Text = string.Format("{0:N0}", WaitForReceivingDataGrid.Items.Count);
                TotalReceivedBalesTextBox.Text = string.Format("{0:N0}", ReceiviedDataGrid.Items.Count);
                TotalShippedBalesTextBox.Text = string.Format("{0:N0}", _truckPackingList.Where(x => x.ShippingCode != null).Count());

                TotalWaitForReceivingTexBlock.Text = "(" + string.Format("{0:N0}", WaitForReceivingDataGrid.Items.Count) + ")";
                TotalReceivedTexBlock.Text = "(" + string.Format("{0:N0}", ReceiviedDataGrid.Items.Count) + ")";

                if (WaitForReceivingDataGrid.Items.Count <= 0)
                    receiveAllButton.IsEnabled = false;
                else
                    receiveAllButton.IsEnabled = true;

                if (ReceiviedDataGrid.Items.Count <= 0)
                    returnAllButton.IsEnabled = false;
                else
                    returnAllButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void Save()
        {
            try
            {
                if (_receiving.ShippingCode != null)
                    throw new ArgumentException("This bale was to shipping already. The system cannot do any things.");

                //Receive.
                if (ReceiveRadioButton.IsChecked == true)
                {
                    if (WarehouseCombobox.SelectedIndex < 0)
                    {
                        MessageBox.Show("Please select a warehouse.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                        WarehouseCombobox.Focus();
                        return;
                    }

                    if (_receiving.TransportationCode != TransportationCodeTextBox.Text)
                        throw new ArgumentException("This bale is in a difference transportation code (" +
                            _receiving.TransportationCode + "). The system cannot receive.");

                    var warehouseId = ((Warehouse)WarehouseCombobox.SelectedItem).WarehouseID;

                    if (_receiving.WarehouseID == null)
                    {
                        /// in case a first receive bale 
                        /// 
                        _receiving.ReceivingWeight = _receiving.BuyingWeight;
                        _receiving.ReceivedBy = sys_config.CurrentUser;
                        _receiving.ReceivedDate = DateTime.Now;
                        _receiving.WarehouseID = warehouseId;
                    }
                    else
                    {
                        /// in case a received bale.
                        /// 
                        Guid? oldWarehouseID = _receiving.WarehouseID;
                        if (_receiving.WarehouseID != oldWarehouseID)
                        {
                            if (MessageBox.Show("This bale is a difference warehouse. Do you want to move and update a new info?",
                                "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
                                return;

                            _receiving.ReceivingWeight = _receiving.BuyingWeight;
                            _receiving.ReceivedBy = sys_config.CurrentUser;
                            _receiving.ReceivedDate = DateTime.Now;
                            _receiving.WarehouseID = warehouseId;
                        }
                        else
                        {
                            if (MessageBox.Show("This bale is already to received.Do you want to replace again?",
                                "confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                                return;

                            _receiving.ReceivingWeight = _receiving.BuyingWeight;
                            _receiving.ReceivedBy = sys_config.CurrentUser;
                            _receiving.ReceivedDate = DateTime.Now;
                        }
                    }
                }
                //Take off.
                else
                {
                    if (_receiving.ShippingCode != null)
                        throw new ArgumentException("This bale aready to shipped. You cannot cancel this bale");

                    _receiving.ReceivedBy = null;
                    _receiving.ReceivedDate = null;
                    _receiving.ReceivingWeight = null;
                    _receiving.Warehouse = null;
                    _receiving.WarehouseID = null;
                }

                BusinessLayerService.ReceivingBLL().Update(_receiving);
                ReloadPage();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ReceiveRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            TakeOffRadioButton.IsChecked = false;
        }

        private void TakeOffRadioButton_Checked(object sender, RoutedEventArgs e)
        {
            ReceiveRadioButton.IsChecked = false;
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key != Key.Enter) return;

            if (BaleBarcodeTextBox.Text.Trim() == "")
            {
                MessageBox.Show("Please input a bale barcode.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                BaleBarcodeTextBox.Focus();
                BaleBarcodeTextBox.Clear();
                return;
            }

            _receiving = BusinessLayerService.ReceivingBLL()
                .GetSingleByBaleBarcode(BaleBarcodeTextBox.Text.Replace(" ", string.Empty));

            if (_receiving == null)
            {
                MessageBox.Show("Find the bale not found!" + Environment.NewLine +
                    "This bale was not imported to the our system. Please check on the truck packing list.",
                    "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

            if (_receiving.TransportationCode != TransportationCodeTextBox.Text)
            {
                MessageBox.Show("The transportation code is difference (The transportation of this bale is " +
                    _receiving.TransportationCode + ") ", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);

                BaleBarcodeTextBox.Clear();
                BaleNumberTextBox.Focus();
                BaleNumberTextBox.Clear();
                return;
            }

            Save();
            BaleBarcodeTextBox.Focus();
        }

        private void BaleNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key != Key.Enter) return;

                if (BaleNumberTextBox.Text == "")
                {
                    MessageBox.Show("Please input a bale number.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleNumberTextBox.Focus();
                    BaleNumberTextBox.Clear();
                    return;
                }

                _receiving = BusinessLayerService.ReceivingBLL()
                    .GetSingleByBaleNumber(Convert.ToInt32(BaleNumberTextBox.Text.Replace(" ", string.Empty)));

                if(_receiving == null)
                {
                    MessageBox.Show("Find the bale not found!" + Environment.NewLine +
                        "This bale was not imported to the our system. Please check on the truck packing list.", 
                        "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (_receiving.TransportationCode != TransportationCodeTextBox.Text)
                {
                    MessageBox.Show("The transportation code is difference (The transportation of this bale is " +
                        _receiving.TransportationCode + ") ", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);

                    BaleBarcodeTextBox.Clear();
                    BaleNumberTextBox.Focus();
                    BaleNumberTextBox.Clear();
                    return;
                }

                Save();
                BaleNumberTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void receiveAllButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to receive a " + _truckPackingList.Where(x => x.ReceivedDate == null).Count() + " bales?",
                    "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                var totalOperatedRecord = 0;
                if (WarehouseCombobox.SelectedIndex < 0)
                {
                    MessageBox.Show("Please select a warehouse.", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    WarehouseCombobox.Focus();
                    return;
                }

                if (_truckPackingList.Where(x => x.ReceivedDate == null).Count() == 0)
                {
                    MessageBox.Show("No data from the left table for receive!", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                var warehouseId = ((Warehouse)WarehouseCombobox.SelectedItem).WarehouseID;

                foreach (var item in _truckPackingList.Where(x => x.ReceivedDate == null))
                {
                    item.ReceivingWeight = _receiving.BuyingWeight;
                    item.ReceivedBy = sys_config.CurrentUser;
                    item.ReceivedDate = DateTime.Now;
                    item.WarehouseID = warehouseId;

                    BusinessLayerService.ReceivingBLL().Update(item);

                    totalOperatedRecord++;
                }

                MessageBox.Show("Received " + totalOperatedRecord + " bales", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                ReloadPage();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void returnAllButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (MessageBox.Show("Do you want to return a " + _truckPackingList.Where(x => x.ReceivedDate != null).Count() + " bales?",
                    "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.No)
                    return;

                if (_truckPackingList.Where(x => x.ShippingCode != null).Count() > 0)
                    throw new ArgumentException("In the right table have a " +
                        _truckPackingList.Where(x => x.ShippingCode != null).Count() +
                        " bales was to shipped already. So, you cannot use this function.");

                var totalOperatedRecord = 0;

                if (_truckPackingList.Where(x => x.ReceivedDate != null).Count() == 0)
                {
                    MessageBox.Show("No data from the right table for return!", "warning!", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                foreach (var item in _truckPackingList.Where(x => x.ReceivedDate != null))
                {
                    item.ReceivingWeight = null;
                    item.ReceivedBy = null;
                    item.ReceivedDate = null;
                    item.Warehouse = null;
                    item.WarehouseID = null;

                    BusinessLayerService.ReceivingBLL().Update(item);

                    totalOperatedRecord++;
                }

                MessageBox.Show("Return " + totalOperatedRecord + " bales", "info", MessageBoxButton.OK, MessageBoxImage.Information);
                ReloadPage();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            BaleBarcodeTextBox.Clear();
        }

        private void BaleNumberTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            BaleNumberTextBox.Clear();
        }
    }
}
