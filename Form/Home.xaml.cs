﻿using AOMInventory.DAL;
using AOMInventory.DomainModel;
using AOMInventory.Form.Receivings;
using AOMInventory.Utility;
using AOMInventory.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMInventory.Form
{
    /// <summary>
    /// Interaction logic for ImportTransportDocumentPage.xaml
    /// </summary>
    public partial class Home : Page
    {
        public Home()
        {
            InitializeComponent();
            ReloadInfo();
        }

        private void ReloadInfo()
        {
            try
            {
                var list = BusinessLayerService.ReceivingBLL().GetByCrop(sys_config.DefaultCrop.Crop1);

                /// Binding info on the summary panels.
                /// 
                importedBaleLabel.Content = list.Count.ToString("N0");
                receiviedBaleLabel.Content = list.Where(x => x.ReceivedDate != null).Count().ToString("N0");
                onHandLabel.Content = list.Where(x => x.ReceivedDate != null && x.ShippingCode == null).Count().ToString("N0");
                shippedBaleLabel.Content = list.Where(x => x.ShippingCode != null).Count().ToString("N0");

                /// Binding info on the datagrid.
                /// 
                transportationDataGrid.ItemsSource = null;
                transportationDataGrid.ItemsSource = Helper.TransportationHelper
                    .GetByCrop(sys_config.DefaultCrop.Crop1);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void importTransportDocumentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                TruckPackingLists window = new TruckPackingLists();
                window.ShowDialog();

                ReloadInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ReloadInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void detailButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (transportationDataGrid.SelectedIndex < 0)
                    return;

                var selectedItem = (vm_TransportationDocument)transportationDataGrid.SelectedItem;
                ReceivingWindow window = new ReceivingWindow(selectedItem.TransportationCode);
                window.ShowDialog();
                ReloadInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}
