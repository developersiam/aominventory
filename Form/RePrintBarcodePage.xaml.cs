﻿using AOMInventory.DAL;
using AOMInventory.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AOMInventory.Form
{
    /// <summary>
    /// Interaction logic for RePrintBarcodePage.xaml
    /// </summary>
    public partial class RePrintBarcodePage : Page
    {
        Receiving _receiving;

        public RePrintBarcodePage()
        {
            InitializeComponent();
            _receiving = new Receiving();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DisplayBaleInfo()
        {
            try
            {
                BaleBarcodeTextBox.Text = _receiving.BaleBarcode;
                BaleNumberTextBox.Text = _receiving.BaleNumber.ToString();
                FarmerIDTextBox.Text = _receiving.FarmerID;
                ProjectTypeTextBox.Text = _receiving.ProjectTypeID;
                BuyingGradeTextBox.Text = _receiving.BuyingGrade;
                BuyingWeightTextBox.Text = string.Format("{0:N2}", _receiving.BuyingWeight);
                TransportationCodeTextBox.Text = _receiving.TransportationCode;
                ImportedDateTextBox.Text = _receiving.ImportDate.ToString();
                ReceivedDateTextBox.Text = _receiving.ReceivedDate.ToString();
                ShippedDateTextBox.Text = _receiving.ShippingToTruckDate.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DisplayBaleBarcodeInfo(string baleBarcode)
        {
            try
            {
                if (baleBarcode == "")
                {
                    MessageBox.Show("Please input a bale barcode.", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Focus();
                    return;
                }

                _receiving = BusinessLayerService.ReceivingBLL().GetSingleByBaleBarcode(baleBarcode);
                if (_receiving == null)
                {
                    MessageBox.Show("Find the bale not found!", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Text = "";
                    BaleBarcodeTextBox.Focus();
                    return;
                }
                DisplayBaleInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void DisplayBaleBarcodeInfoByBaleNumber(short baleNumber)
        {
            try
            {
                _receiving = BusinessLayerService.ReceivingBLL().GetSingleByBaleNumber(baleNumber);
                if (_receiving == null)
                {
                    MessageBox.Show("Find the bale not found!", "Message", MessageBoxButton.OK, MessageBoxImage.Warning);
                    BaleBarcodeTextBox.Text = "";
                    BaleNumberTextBox.Text = "";
                    BaleNumberTextBox.Focus();
                    return;
                }
                DisplayBaleInfo();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                BaleBarcodeTextBox.Text = "";
                BaleNumberTextBox.Text = "";
                FarmerIDTextBox.Text = "";
                ProjectTypeTextBox.Text = "";
                BuyingGradeTextBox.Text = "";
                BuyingWeightTextBox.Text = "";
                TransportationCodeTextBox.Text = "";
                ImportedDateTextBox.Clear();
                ReceivedDateTextBox.Clear();
                ShippedDateTextBox.Clear();
                BaleBarcodeTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void PrintBarcodeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Utility.PrintBarcode _printBarcode = new Utility.PrintBarcode();
                _printBarcode.PrintBarcodeSticker(Convert.ToInt16(BaleNumberTextBox.Text.Replace(" ", string.Empty)));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleBarcodeTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                    DisplayBaleBarcodeInfo(BaleBarcodeTextBox.Text.Replace(" ", string.Empty));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void BaleNumberTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                if (e.Key == Key.Enter)
                    DisplayBaleBarcodeInfoByBaleNumber(Convert.ToInt16(BaleNumberTextBox.Text.Replace(" ", string.Empty)));
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
    }
}