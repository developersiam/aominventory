﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMInventory.DAL
{
    public class BusinessLayerService
    {
        public static IReceivingBLL ReceivingBLL()
        {
            IReceivingBLL bll = new ReceivingBLL();
            return bll;
        }
        public static IShippingDocument ShippingDocumentBLL()
        {
            IShippingDocument bll = new ShippingDocumentBLL();
            return bll;
        }
        public static IShippingBLL ShippingBLL()
        {
            IShippingBLL bll = new ShippingBLL();
            return bll;
        }
        public static IWarehouseBLL WarehouseBLL()
        {
            IWarehouseBLL bll = new WarehouseBLL();
            return bll;
        }
        public static ICropBLL CropBLL()
        {
            ICropBLL bll = new CropBLL();
            return bll;
        }

    }
}
