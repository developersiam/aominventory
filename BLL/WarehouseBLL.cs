﻿using AOMInventory.DomainModel;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMInventory.DAL
{
    public interface IWarehouseBLL
    {
        void Add(string warehouseName);
        void Edit(Warehouse model);
        void Delete(Guid warehouseID);
        List<Warehouse> GetAll();
        Warehouse GetSingle(Guid warehouseID);
    }

    public class WarehouseBLL : IWarehouseBLL
    {
        UnitOfWork _uow;

        public WarehouseBLL()
        {
            _uow = new UnitOfWork();
        }

        public void Add(string warehouseName)
        {
            try
            {
                if (warehouseName == null)
                    throw new ArgumentException("A warehouse name not found.");

                if (_uow.WarehouseRepository.GetSingle(w => w.WarehouseName == warehouseName) != null)
                    throw new ArgumentException("This warehouse name is already in the system.");

                Warehouse warehouse = new Warehouse
                {
                    WarehouseID = Guid.NewGuid(),
                    WarehouseName = warehouseName,
                    ModifiedBy = sys_config.CurrentUser,
                    ModifiedDate = DateTime.Now
                };

                _uow.WarehouseRepository.Add(warehouse);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid warehouseID)
        {
            try
            {
                Warehouse warehouse = new Warehouse();
                warehouse = _uow.WarehouseRepository.GetSingle(w => w.WarehouseID == warehouseID, w => w.Receivings);

                if (warehouse == null)
                    throw new ArgumentException("Find a data not found.");

                if (warehouse.Receivings.Count > 0)
                    throw new ArgumentException("This warehouse have many bale.The system cannot delete.");

                _uow.WarehouseRepository.Remove(warehouse);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(Warehouse model)
        {
            try
            {
                if (model.WarehouseName == null)
                    throw new ArgumentException("A warehouse name not found.");

                Warehouse warehouse = new Warehouse();
                warehouse = _uow.WarehouseRepository.GetSingle(w => w.WarehouseID == model.WarehouseID);

                if (warehouse == null)
                    throw new ArgumentException("Find a data not found.");

                warehouse.WarehouseName = model.WarehouseName;
                model.ModifiedBy = sys_config.CurrentUser;
                model.ModifiedDate = DateTime.Now;

                _uow.WarehouseRepository.Update(model);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Warehouse> GetAll()
        {
            return _uow.WarehouseRepository.Get().ToList();
        }

        public Warehouse GetSingle(Guid warehouseID)
        {
            return _uow.WarehouseRepository.GetSingle(w => w.WarehouseID == warehouseID);
        }
    }
}
