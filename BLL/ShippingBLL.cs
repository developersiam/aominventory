﻿using AOMInventory.DomainModel;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMInventory.DAL
{
    public interface IShippingBLL
    {
        void BringUp(string baleBarcode, string recordingUser, string shippingCode);
        void BringDown(string baleBarcode);
    }
    public class ShippingBLL : IShippingBLL
    {
        UnitOfWork _uow;
        public ShippingBLL()
        {
            _uow = new UnitOfWork();
        }
        public void BringUp(string baleBarcode, string recordingUser, string shippingCode)
        {
            try
            {
                if (baleBarcode == null || baleBarcode == "")
                    throw new ArgumentException("Bale barcode not found.");

                if (recordingUser == null)
                    throw new ArgumentException("Recording user not found.");

                if (shippingCode == null)
                    throw new ArgumentException("Shipping code not found.");

                Receiving receiving = new Receiving();
                receiving = _uow.ReceivingRepository.GetSingle(b => b.BaleBarcode == baleBarcode);

                receiving.ShippingCode = shippingCode;
                receiving.ShippingToTruckDate = DateTime.Now;
                receiving.ShippingToTruckUser = recordingUser;

                _uow.ReceivingRepository.Update(receiving);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void BringDown(string baleBarcode)
        {
            try
            {
                if (baleBarcode == null || baleBarcode == "")
                    throw new ArgumentException("Bale barcode not found.");

                Receiving receiving = new Receiving();
                receiving = _uow.ReceivingRepository
                    .GetSingle(b => b.BaleBarcode == baleBarcode);

                receiving.ShippingDocument = null;
                receiving.ShippingCode = null;
                receiving.ShippingToTruckDate = null;
                receiving.ShippingToTruckUser = null;

                _uow.ReceivingRepository.Update(receiving);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
