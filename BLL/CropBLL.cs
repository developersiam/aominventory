﻿using AOMInventory.DomainModel;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMInventory.DAL
{
    public interface ICropBLL
    {
        Crop GetDefaultCrop();
        List<Crop> GetAll();
    }

    public class CropBLL : ICropBLL
    {
        UnitOfWork _uow;

        public CropBLL()
        {
            _uow = new UnitOfWork();
        }

        public List<Crop> GetAll()
        {
            return _uow.CropRepository.Get();
        }

        public Crop GetDefaultCrop()
        {
            return _uow.CropRepository.GetSingle(c => c.DefaultStatus == true);
        }
    }
}
