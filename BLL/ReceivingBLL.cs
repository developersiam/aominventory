﻿using System;
using System.Collections.Generic;
using System.Linq;
using Spire.Xls;
using System.Data;
using AOMInventory.DomainModel;
using AOMInventory.ViewModel;
using DAL;

namespace AOMInventory.DAL
{
    public interface IReceivingBLL
    {
        void Add(Receiving receiving);

        void Update(Receiving receiving);

        void Delete(Receiving receiving);

        List<Receiving> GetByTransportationCode(string transportationCode);

        Receiving GetSingleByBaleBarcode(string baleBarcode);

        Receiving GetSingleByBaleNumber(int baleNumber);

        List<Receiving> GetByShippingCode(string shippingCode);

        List<Receiving> GetByCrop(short crop);

        List<Receiving> BaleBarcodeEmpty(List<Receiving> collections);

        List<Receiving> GetByWarehouse(short crop, string type, Guid WarehouseID);
    }

    public class ReceivingBLL : IReceivingBLL
    {
        UnitOfWork _uow;
        public ReceivingBLL()
        {
            _uow = new UnitOfWork();
        }

        public void Add(Receiving receiving)
        {
            try
            {
                if (receiving.BaleBarcode == null || receiving.BaleBarcode == "")
                    throw new ArgumentException("BaleBarcode not found.");

                _uow.ReceivingRepository.Add(receiving);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(Receiving receiving)
        {
            try
            {
                if (receiving.BaleBarcode == null || receiving.BaleBarcode == "")
                    throw new ArgumentException("BaleBarcode not found.");

                _uow.ReceivingRepository.Update(receiving);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Receiving receiving)
        {
            try
            {
                if (receiving.BaleBarcode == null || receiving.BaleBarcode == "")
                    throw new ArgumentException("BaleBarcode not found.");

                if (receiving.ReceivedBy == null || receiving.ReceivedBy == "")
                    throw new ArgumentException("Receiving user not found.");

                Receiving deleteModel = new Receiving();
                deleteModel = GetSingleByBaleBarcode(receiving.BaleBarcode);

                if (deleteModel == null)
                    throw new ArgumentException("Find data not found.");

                if (deleteModel.ReceivedDate != null)
                    throw new ArgumentException("This bale was received already (on " + deleteModel.ReceivedDate + ").Please check again.");

                if (deleteModel.ShippingCode != null)
                    throw new ArgumentException("This bale was shipping to port already (on " + deleteModel.ReceivedDate + ").Please check again.");

                deleteModel.ReceivedDate = DateTime.Now;
                deleteModel.ReceivedBy = receiving.ReceivedBy;

                _uow.ReceivingRepository.Remove(deleteModel);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Receiving> GetByTransportationCode(string transportationCode)
        {
            return _uow.ReceivingRepository
                .Get(r => r.TransportationCode == transportationCode,
                null,
                r => r.Warehouse,
                r => r.ShippingDocument).ToList();
        }

        public Receiving GetSingleByBaleBarcode(string baleBarcode)
        {
            return _uow.ReceivingRepository.GetSingle(r => r.BaleBarcode == baleBarcode, 
                r => r.ShippingDocument,
                r => r.Warehouse);
        }

        public Receiving GetSingleByBaleNumber(int baleNumber)
        {
            return _uow.ReceivingRepository.GetSingle(r => r.BaleNumber == baleNumber, 
                r => r.ShippingDocument,
                r=>r.Warehouse);
        }

        public List<Receiving> GetByShippingCode(string shippingCode)
        {
            return _uow.ReceivingRepository.Get(r => r.ShippingCode == shippingCode,
                null, 
                r => r.ShippingDocument,
                r => r.Warehouse).ToList();
        }

        public List<Receiving> GetByCrop(short crop)
        {
            return _uow.ReceivingRepository
                .Get(x => x.Crop == crop, null,
                x => x.ShippingDocument,
                x => x.Warehouse).ToList();
        }

        public List<Receiving> BaleBarcodeEmpty(List<Receiving> Collection)
        {
            var result = Collection.Where(w => w.BaleBarcode == string.Empty).ToList();
            return result;
        }

        public List<Receiving> GetByWarehouse(short crop, string type, Guid WarehouseID)
        {
            return _uow.ReceivingRepository
                .Get(r => r.Crop == crop &&
                r.ProjectTypeID == type &&
                r.WarehouseID == WarehouseID,
                null,
                r => r.ShippingDocument)
                .OrderBy(o => o.BaleNumber)
                .Take(150)
                .ToList();
        }
    }
}
