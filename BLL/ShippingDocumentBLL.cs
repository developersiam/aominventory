﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AOMInventory.DAL;
using AOMInventory.DomainModel;
using DAL;

namespace AOMInventory.DAL
{
    public interface IShippingDocument
    {
        string Add(short crop, string truckNo, DateTime createDate, string createUser);
        void Update(ShippingDocument shippingDocument);
        void Delete(string shippingCode);
        List<ShippingDocument> GetByCreateDate(DateTime createDate);
        List<ShippingDocument> GetAll(short crop);
        ShippingDocument GetSingle(string shippingCode);
        void FinishDocument(string shippingCode);
        void UnFinishDocument(string shippingCode);
    }

    public class ShippingDocumentBLL : IShippingDocument
    {
        UnitOfWork _uow;

        public ShippingDocumentBLL()
        {
            _uow = new UnitOfWork();
        }

        public string Add(short crop, string truckNo, DateTime createDate, string createUser)
        {
            try
            {
                if (createDate == null)
                    throw new ArgumentException("Create date not found.");

                if (createUser == null)
                    throw new ArgumentException("Create user not found.");

                if (truckNo == null || truckNo == "")
                    throw new ArgumentException("Truck number not found.");

                var list = BusinessLayerService
                    .ShippingDocumentBLL()
                    .GetAll(crop)
                    .Select(x => new
                    {
                        RunNumber = Convert.ToInt16(x.ShippingCode
                        .Substring(x.ShippingCode.Length - 3, 3))
                    })
                    .ToList();

                var max = list.Max(x => x.RunNumber);

                var shippingCode = "SHP-" +
                    createDate.Year +
                    createDate.Month.ToString().PadLeft(2, '0') +
                    createDate.Day.ToString().PadLeft(2, '0') +
                    "-" +
                    (max + 1).ToString().PadLeft(3, '0');

                _uow.ShippingDocumentRepository
                    .Add(new ShippingDocument
                    {
                        ShippingCode = shippingCode,
                        Crop = crop,
                        TruckNo = truckNo,
                        CreateDate = createDate,
                        CreateUser = createUser,
                        IsFinish = false,
                        FinishDate = null,
                        ModifiedDate = DateTime.Now,
                        ModifiedUser = createUser
                    });
                _uow.Save();

                return shippingCode;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Update(ShippingDocument shippingDocument)
        {
            try
            {
                if (shippingDocument.TruckNo == null || shippingDocument.TruckNo == "")
                    throw new ArgumentException("Truck number not found.");

                var model = GetSingle(shippingDocument.ShippingCode);

                if (model == null)
                    throw new ArgumentException("Find data not found.");

                shippingDocument.ModifiedDate = DateTime.Now;
                shippingDocument.ModifiedUser = sys_config.CurrentUser;

                _uow.ShippingDocumentRepository.Update(shippingDocument);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void Delete(string shippingCode)
        {
            try
            {
                ShippingDocument currentShippingDocument = new ShippingDocument();
                currentShippingDocument = GetSingle(shippingCode);

                if (currentShippingDocument == null)
                    throw new ArgumentException("Find data not found.");

                if (currentShippingDocument.IsFinish == true)
                    throw new ArgumentException("This document was set to finish. The system not allow to delete.");

                if (currentShippingDocument.Receivings.Count > 0)
                    throw new ArgumentException("This document have many bales was shipped already. The system cannot delete.");

                _uow.ShippingDocumentRepository.Remove(currentShippingDocument);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public List<ShippingDocument> GetByCreateDate(DateTime createDate)
        {
            return _uow.ShippingDocumentRepository.Get(s => s.CreateDate == createDate, null, s => s.Receivings).ToList();
        }
        public List<ShippingDocument> GetAll(short crop)
        {
            return _uow.ShippingDocumentRepository.Get(s => s.Crop == crop, null, s => s.Receivings).ToList();
        }
        public ShippingDocument GetSingle(string shippingCode)
        {
            return _uow.ShippingDocumentRepository
                .GetSingle(s => s.ShippingCode == shippingCode, s => s.Receivings);
        }
        public void FinishDocument(string shippingCode)
        {
            try
            {
                if (shippingCode == null) throw new ArgumentException("A shippingDocument ID is not set.");

                ShippingDocument shippingDocument = new ShippingDocument();
                shippingDocument = GetSingle(shippingCode);

                if (shippingDocument == null)
                    throw new ArgumentException("Find data not found.");

                shippingDocument.IsFinish = true;
                shippingDocument.FinishDate = DateTime.Now;
                shippingDocument.ModifiedDate = DateTime.Now;

                _uow.ShippingDocumentRepository.Update(shippingDocument);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void UnFinishDocument(string shippingCode)
        {
            try
            {
                if (shippingCode == null)
                    throw new ArgumentException("A shippingDocument ID not found.");

                ShippingDocument shippingDocument = new ShippingDocument();
                shippingDocument = GetSingle(shippingCode);

                if (shippingDocument == null)
                    throw new ArgumentException("Find data not found.");

                shippingDocument.IsFinish = false;
                shippingDocument.FinishDate = null;
                shippingDocument.ModifiedDate = DateTime.Now;

                _uow.ShippingDocumentRepository.Update(shippingDocument);
                _uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
