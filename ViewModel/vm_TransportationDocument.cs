﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AOMInventory.ViewModel
{
    public class vm_TransportationDocument
    {
        public short Crop { get; set; }
        public Double TotalWeight { get; set; }
        public Double TotalBales { get; set; }
        public Double Received { get; set; }
        public Double NotReceived { get; set; }
        public Double TotalShipped { get; set; }
        public string TransportationCode { get; set; }
        public string TransportationTruckNo { get; set; }
    }
}
