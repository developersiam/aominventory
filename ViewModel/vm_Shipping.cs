﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMInventory.DomainModel;

namespace AOMInventory.ViewModel
{
    public class vm_Shipping : Receiving
    {
        public string ShippingTruckNo { get; set; }
        public string WarehouseName { get; set; }
    }
}
