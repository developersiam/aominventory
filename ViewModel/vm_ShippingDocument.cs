﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AOMInventory.DomainModel;

namespace AOMInventory.ViewModel
{
    public class vm_ShippingDocument : ShippingDocument
    {
        public Double TotalWeight { get; set; }
        public Double TotalBales { get; set; }
    }
}
